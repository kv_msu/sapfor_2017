#include "loops_combiner.h"

#include "../LoopAnalyzer/loop_analyzer.h"
#include "../ExpressionTransform/expr_transform.h"
#include "../Utils/errors.h"
#include <string>
#include <vector>
#include <queue>

using std::string;
using std::vector;
using std::map;
using std::set;
using std::pair;
using std::make_pair;
using std::queue;
using std::wstring;

static SgSymbol* getLoopSymbol(const LoopGraph* loop)
{
    if (!loop)
        return NULL;

    SgForStmt* stmt = (SgForStmt*)loop->loop->GetOriginal();
    return stmt->doName();
}

static void fillIterationVariables(const LoopGraph* loop, set<SgSymbol*>& vars, int dimensions = -1)
{
    if (dimensions == -1)
    {
        vars.insert(getLoopSymbol(loop));
        for (LoopGraph* child : loop->children)
            fillIterationVariables(child, vars);
    }
    else
    {
        for (int i = 0; i < dimensions; ++i)
        {
            vars.insert(getLoopSymbol(loop));
            if (i != dimensions - 1)
                loop = loop->children[0];
        }
    }
}

static void eraseSymbolById(set<SgSymbol*>& symbols, int id)
{
    SgSymbol* toDelete = NULL;
    for (SgSymbol* elem : symbols)
    {
        if (elem->id() == id)
        {
            toDelete = elem;
            break;
        }
    }
    if (toDelete)
        symbols.erase(toDelete);
}

static SgSymbol* findSymbolById(const set<SgSymbol*>& symbols, int id)
{
    for (SgSymbol* elem : symbols)
        if (elem->id() == id)
            return elem;

    return NULL;
}

static void getIntersectionById(const set<SgSymbol*>& firstSet, const set<SgSymbol*>& secondSet, set<SgSymbol*>& intersection)
{
    for (SgSymbol* var1 : firstSet)
    {
        for (SgSymbol* var2 : secondSet)
        {
            if (var1->id() == var2->id())
            {
                intersection.insert(var1);
                break;
            }
        }
    }
}

static bool expressionsAreEqual(SgExpression* exp1, SgExpression* exp2)
{
    if (exp1 == NULL && exp2 == NULL)
        return true;

    if (exp1 != NULL && exp2 != NULL)
    {
        string str1 = exp1->unparse();
        string str2 = exp2->unparse();
        return str1 == str2;
    }

    return false;
}

static bool ifLoopCanBeReversed(const LoopGraph* loop)
{
    const set<string> privVars;
    depGraph* dependency = getDependenciesGraph(loop, current_file, &privVars);
    vector<depNode*> nodes = dependency->getNodes();
    for (depNode* node : nodes)
    {
        int type = node->typedep;
        int kind = node->kinddep;

        // ARRAYDEP and (OUTPUT or REDUCE):
        if (type == 1 && (kind == 2 || kind == 3))
            continue;

        // PRIVATEDEP:
        if (type == 2)
            continue;

        return false;
    }

    return true;
}

static void reverseLoop(LoopGraph* loop, int dimensions)
{
    if (loop == NULL)
        return;

    for (int i = 0; i < dimensions; ++i)
    {
        std::swap(loop->startVal, loop->endVal);
        loop->stepVal *= -1;

        SgForStmt* loopStmt = (SgForStmt*)(loop->loop->GetOriginal());
        SgExpression* tmpEx = loopStmt->start();
        loopStmt->setStart(*loopStmt->end());
        loopStmt->setEnd(*tmpEx);

        tmpEx = loopStmt->step();
        if (tmpEx == NULL)
            tmpEx = new SgValueExp(1);

        if (tmpEx->variant() == MINUS_OP)
        {
            SgExpression* lhs = tmpEx->lhs();
            loopStmt->setStep(*lhs);
        }
        else
            loopStmt->setStep(*(new SgExpression(MINUS_OP, tmpEx, NULL)));

        if (i != dimensions - 1)
            loop = loop->children[0];
    }
}

static int getDipestDimToReverse(LoopGraph* firstLoop, LoopGraph* loop, int perfectLoop, LoopGraph** toReverse)
{
    LoopGraph* curFisrtLoop = firstLoop;
    LoopGraph* curLoop = loop;
    int i = 0;

    bool canBeReversed1 = ifLoopCanBeReversed(firstLoop);
    bool canBeReversed2 = ifLoopCanBeReversed(loop);
    if (!canBeReversed1 && !canBeReversed2)
        return 0;

    for (i = 0; i < perfectLoop; ++i)
    {
        if (curLoop->hasLimitsToSplit())
            break;

        SgForStmt* firstLoopStmt = (SgForStmt*)(curFisrtLoop->loop->GetOriginal());
        SgForStmt* loopStmt = (SgForStmt*)(curLoop->loop->GetOriginal());

        if (!expressionsAreEqual(firstLoopStmt->start(), loopStmt->end()))
            break;
        if (!expressionsAreEqual(firstLoopStmt->end(), loopStmt->start()))
            break;

        SgExpression* step1 = firstLoopStmt->step();
        SgExpression* step2 = loopStmt->step();
        SgExpression* defaultStep = new SgValueExp(1);
        if (step1 == NULL)
            step1 = defaultStep;
        if (step2 == NULL)
            step2 = defaultStep;

        if (!((step1->variant() == MINUS_OP) ^ (step2->variant() == MINUS_OP)))
            break;

        if (step1->variant() == MINUS_OP && !expressionsAreEqual(step1->lhs(), step2))
            break;
        if (step2->variant() == MINUS_OP && !expressionsAreEqual(step1, step2->lhs()))
            break;

        if (i != perfectLoop - 1)
        {
            if (curLoop->children.size() != 1)
                break;

            curFisrtLoop = curFisrtLoop->children[0];
            curLoop = curLoop->children[0];
        }
    }

    if (i > 0)
    {
        if (canBeReversed1)
            *toReverse = firstLoop;
        else
            *toReverse = loop;
    }

    return i;
}

/**
  *  Найти количество измерений, объединение по которым возможно.
  */
static int getDeepestDimForCombine(const vector<SgForStmt*>& basisLoops, const LoopGraph* loop)
{
    const LoopGraph* curLoop = loop;
    int i = 0;

    for (i = 0; i < basisLoops.size(); ++i)
    {
        if (curLoop->hasLimitsToSplit())
            return i;

        SgForStmt* loopStmt = (SgForStmt*)(curLoop->loop->GetOriginal());
        if (!expressionsAreEqual(basisLoops[i]->start(), loopStmt->start()))
            return i;

        if (!expressionsAreEqual(basisLoops[i]->end(), loopStmt->end()))
            return i;

        SgExpression* step1 = basisLoops[i]->step();
        SgExpression* step2 = loopStmt->step();

        if (!expressionsAreEqual(step1, step2))
        {
            if ((step1 == NULL) ^ (step2 == NULL))
            {
                SgExpression* defaultStep = new SgValueExp(1);
                if (step1 == NULL)
                    step1 = defaultStep;
                else
                    step2 = defaultStep;

                if (!expressionsAreEqual(step1, step2))
                    return i;
            }
            else
                return i;
        }

        if (i != basisLoops.size() - 1)
        {
            if (curLoop->children.size() != 1)
                return i;

            curLoop = curLoop->children[0];
        }
    }

    return i;
}

static void compareIterationVars(const LoopGraph* firstLoop, const LoopGraph* loop, int dimensions, map<SgSymbol*, SgSymbol*>& symbols)
{
    for (int i = 0; i < dimensions; ++i)
    {
        SgForStmt* firstLoopStmt = (SgForStmt*)(firstLoop->loop->GetOriginal());
        SgForStmt* loopStmt = (SgForStmt*)(loop->loop->GetOriginal());

        if (string(firstLoopStmt->doName()->identifier()) != string(loopStmt->doName()->identifier()))
            symbols.insert(make_pair(loopStmt->doName(), firstLoopStmt->doName()));

        if (i != dimensions - 1)
        {
            firstLoop = firstLoop->children[0];
            loop = loop->children[0];
        }
    }
}

static SgSymbol* copySymbolAndRename(SgSymbol* symbol)
{
    string baseName = symbol->identifier();

    size_t pos = baseName.rfind('_');
    string strNumber;
    int number;
    if (pos != string::npos)
    {
        for (size_t i = pos + 1; i < baseName.length(); ++i)
        {
            if (baseName[i] >= '0' && baseName[i] <= '9')
                strNumber.push_back(baseName[i]);
            else
            {
                strNumber.clear();
                break;
            }
        }
    }

    if (!strNumber.empty())
    {
        baseName.resize(baseName.length() - (strNumber.length() + 1));
        number = atoi(strNumber.c_str()) + 1;
    }
    else
        number = 1;

    int new_name_num = checkSymbNameAndCorrect(baseName + '_', number);
    string new_name = baseName + '_' + std::to_string(new_name_num);

    SgSymbol* new_sym = &symbol->copy();
    new_sym->changeName(new_name.c_str());

    return new_sym;
}

static void renameVariables(const map<SgSymbol*, SgSymbol*>& symbols, SgExpression* ex)
{
    if (ex)
    {
        if ((ex->variant() == VAR_REF || ex->variant() == ARRAY_REF) && ex->symbol())
        {
            int symbol_id = ex->symbol()->id();
            for (auto& pair : symbols)
            {
                if (pair.first->id() == symbol_id)
                {
                    ex->setSymbol(pair.second);
                    break;
                }
            }
        }

        renameVariables(symbols, ex->lhs());
        renameVariables(symbols, ex->rhs());
    }
}

static void renameIterationVariables(LoopGraph* loop, const map<SgSymbol*, SgSymbol*>& symbols)
{
    if (loop)
    {
        string& loopName = loop->loopSymbol;
        for (auto& pair : symbols)
        {
            if (pair.first->identifier() == loopName)
            {
                loop->loopSymbol = (string)pair.second->identifier();
                break;
            }
        }

        for (LoopGraph* child : loop->children)
            renameIterationVariables(child, symbols);
    }
}

static void renameVariablesInLoop(LoopGraph* loop, const map<SgSymbol*, SgSymbol*>& symbols)
{
    renameIterationVariables(loop, symbols);

    for (SgStatement* st = loop->loop; st != loop->loop->lastNodeOfStmt(); st = st->lexNext())
    {
        if (st->variant() == FOR_NODE)
        {
            SgForStmt* for_st = (SgForStmt*)st;
            for (auto& pair : symbols)
                if (pair.first->id() == for_st->symbol()->id())
                    for_st->setDoName(*pair.second);
        }

        for (int i = 0; i < 3; ++i)
            renameVariables(symbols, st->expr(i));
    }
}

static void renamePrivatesInMap(LoopGraph* loop, const map<SgSymbol*, SgSymbol*>& symbols, map<LoopGraph*, set<SgSymbol*>>& mapPrivates)
{
    auto privates = mapPrivates.find(loop);
    if (loop && privates != mapPrivates.end())
    {
        for (auto& pair : symbols)
            if (privates->second.erase(pair.first))
                privates->second.insert(pair.second);

        for (LoopGraph* child : loop->children)
            renamePrivatesInMap(child, symbols, mapPrivates);
    }
}

static void addIterationVarsToMap(LoopGraph* loop, map<LoopGraph*, set<SgSymbol*>>& mapPrivates)
{
    auto privates = mapPrivates.find(loop);
    if (loop && privates != mapPrivates.end())
    {
        set<SgSymbol*> symbols;
        fillIterationVariables(loop, symbols);
        for (SgSymbol* var : symbols)
            if (findSymbolById(privates->second, var->id()) == NULL)
                privates->second.insert(var);

        for (LoopGraph* child : loop->children)
            addIterationVarsToMap(child, mapPrivates);
    }
}

static void fillMapPrivateVars(const vector<LoopGraph*>& loopGraphs, map<LoopGraph*, set<SgSymbol*>>& mapPrivates)
{
    if (loopGraphs.size() == 0)
        return;

    for (int i = 0; i < loopGraphs.size(); ++i)
    {
        LoopGraph* loop = loopGraphs[i];

        set<Symbol*> symbols;
        for (auto& data : getAttributes<SgStatement*, SgStatement*>(loop->loop, set<int>{ SPF_ANALYSIS_DIR }))
            fillPrivatesFromComment(new Statement(data), symbols);

        set<SgSymbol*> loopPrivates;
        for (Symbol* symbol : symbols)
            loopPrivates.insert(OriginalSymbol((SgSymbol*)symbol));

        mapPrivates.insert(make_pair(loop, loopPrivates));

        if (!loop->children.empty())
            fillMapPrivateVars(loop->children, mapPrivates);
    }
}

static SgForStmt* getInnerLoop(const LoopGraph* loop, int deep)
{
    int perfectLoop = loop->perfectLoop;
    const LoopGraph* curLoop = loop;
    SgForStmt* result = NULL;

    for (int i = 0; i < deep; ++i)
    {
        result = (SgForStmt*)(curLoop->loop->GetOriginal());
        if (i != perfectLoop - 1)
            curLoop = curLoop->children[0];
    }

    return result;
}

static void moveBody(SgForStmt* from, SgForStmt* to, const map<SgSymbol*, SgSymbol*>& symbols)
{
    for (auto st = from->lexNext(); st != from->lastNodeOfStmt(); st = st->lexNext())
        for (int i = 0; i < 3; ++i)
            renameVariables(symbols, st->expr(i));

    auto loopBody = from->extractStmtBody();
    to->lastExecutable()->insertStmtAfter(*loopBody, *to);
}

static SgExpression* createIterationCountExpr(const LoopGraph* loop)
{
    // loop: do i = a, b, c
    // iteration count expression: (a + ((b - a + c) / c - 1) * c)

    SgForStmt* firstLoopStmt = (SgForStmt*)(loop->loop->GetOriginal());
    SgExpression* a = firstLoopStmt->start();
    SgExpression* b = firstLoopStmt->end();
    SgExpression* c = firstLoopStmt->step();

    if (c == NULL)
        c = new SgValueExp(1);

    SgExpression* ex = &(*a + ((*b - *a + *c) / *c - *new SgValueExp(1)) * *c);
    return ex;
}

static void changeVarToExpr(SgExpression* expression, const SgSymbol* var, SgExpression* changeExpr)
{
    if (expression == NULL || var == NULL)
        return;

    int varId = var->id();
    SgExpression* lhs = expression->lhs();
    SgExpression* rhs = expression->rhs();

    if (lhs && lhs->symbol() && lhs->symbol()->id() == varId)
        expression->setLhs(changeExpr);

    if (rhs && rhs->symbol() && rhs->symbol()->id() == varId)
        expression->setRhs(changeExpr);

    changeVarToExpr(expression->lhs(), var, changeExpr);
    changeVarToExpr(expression->rhs(), var, changeExpr);
}

static void changeVarToExpr(SgStatement* statement, const SgSymbol* var, SgExpression* expr, int startExpr = 0)
{
    if (statement == NULL || var == NULL)
        return;

    int varId = var->id();
    for (int i = startExpr; i < 3; ++i)
    {
        SgExpression* ex = statement->expr(i);
        if (ex && ex->symbol() && ex->symbol()->id() == varId)
        {
            statement->setExpression(i, expr);
            continue;
        }

        changeVarToExpr(ex, var, expr);
    }
}

static void changeIterationVarToCountExpr(LoopGraph* firstLoop, LoopGraph* loop, int dimensions, const SgSymbol* var)
{
    SgSymbol* sym = NULL;

    int varId = var->id();
    for (int dim = 0; dim < dimensions; ++dim)
    {
        sym = getLoopSymbol(firstLoop);
        if (sym->id() == varId)
            break;

        firstLoop = firstLoop->children[0];
    }

    SgExpression* countExpr = createIterationCountExpr(firstLoop);
    for (SgStatement* st = loop->loop; st != loop->loop->lastNodeOfStmt(); st = st->lexNext())
        changeVarToExpr(st, var, countExpr);
}

static bool isVarInExpression(const SgSymbol* var, SgExpression* ex)
{
    bool res = false;
    if (ex)
    {
        if (ex->symbol() && ex->symbol()->id() == var->id())
            return true;

        res |= isVarInExpression(var, ex->lhs());
        res |= isVarInExpression(var, ex->rhs());
    }

    return res;
}

static bool varIsChanged(const SgSymbol* var, LoopGraph* loop)
{
    for (SgStatement* st = loop->loop; st != loop->loop->lastNodeOfStmt(); st = st->lexNext())
        if (st->variant() == ASSIGN_STAT && st->expr(0)->symbol()->id() == var->id())
            return true;
    
    return false;
}

static bool varIsRead(const SgSymbol* var, LoopGraph* loop)
{
    for (SgStatement* st = loop->loop; st != loop->loop->lastNodeOfStmt(); st = st->lexNext())
    {
        int i = 0;
        if (st->variant() == ASSIGN_STAT && st->expr(0)->symbol()->id() == var->id())
            i = 1;

        for (; i < 3; ++i)
            if (st->expr(i) && isVarInExpression(var, st->expr(i)) != NULL)
                return true;
    }

    return false;
}

static bool varIsChangedBetween(const SgSymbol* var, SgStatement* begin, SgStatement* end)
{
    for (SgStatement* st = begin; st != end; st = st->lexNext())
        if (st->variant() == ASSIGN_STAT && st->expr(0)->symbol()->id() == var->id())
            return true;

    return false;
}

static bool varIsUsedBetween(const SgSymbol* var, SgStatement* begin, SgStatement* end)
{
    if (begin == NULL || end == NULL)
        return false;

    for (SgStatement* st = begin; st != end; st = st->lexNext())
        for (int i = 0; i < 3; ++i)
            if (st->expr(i) && isVarInExpression(var, st->expr(i)))
                return true;

    return false;
}

static bool isAntiVarDependency(const SgSymbol* var, SgForStmt* loop)
{
    bool is_used = false;
    for (SgStatement* st = loop; st != loop->lastNodeOfStmt(); st = st->lexNext())
    {
        if (st->variant() == ASSIGN_STAT && st->expr(0)->symbol()->id() == var->id())
        {
            for (int i = 1; i < 3; ++i)
                if (st->expr(i) && isVarInExpression(var, st->expr(i)))
                    return true;

            return is_used;
        }
        
        for (int i = 0; i < 3; ++i)
            if (st->expr(i) && isVarInExpression(var, st->expr(i)))
                is_used = true;
    }

    return false;
}

static void replaceIterationVar(LoopGraph* firstLoop, LoopGraph* loop, int dimensions, SgSymbol* var, SgSymbol* newSymbol)
{
    LoopGraph* first = firstLoop;
    for (int i = 0; i < dimensions; ++i)
    {
        SgSymbol* loopSymbol = getLoopSymbol(first);
        if (loopSymbol->id() == var->id())
            break;

        first = first->children[0];
    }
    SgExpression* countExpr = createIterationCountExpr(first);
    SgStatement* st = new SgAssignStmt(*new SgVarRefExp(newSymbol), *countExpr);
    firstLoop->loop->insertStmtBefore(*st, *firstLoop->loop->controlParent());

    map<SgSymbol*, SgSymbol*> toRename;
    toRename.insert(make_pair(var, newSymbol));
    renameVariablesInLoop(loop, toRename);
}

static bool varIsReallyNotPrivate(const SgSymbol* var, const LoopGraph* loop, map<LoopGraph*, set<SgSymbol*>>& mapPrivates)
{
    bool res = false;
    for (SgStatement* st = loop->loop; st != loop->loop->lastNodeOfStmt(); st = st->lexNext())
    {
        if (st->variant() == FOR_NODE)
        {
            for (LoopGraph* child : loop->children)
            {
                if (child->loop->id() == st->id())
                {
                    if (findSymbolById(mapPrivates[child], var->id()) != NULL)
                        res = false;
                    else
                        res = varIsReallyNotPrivate(var, child, mapPrivates);

                    st = st->lastNodeOfStmt();
                }
            }
        }
        else
        {
            for (int i = 0; i < 3; ++i)
                if (st->expr(i) && isVarInExpression(var, st->expr(i)))
                    return true;
        }
    }

    return res;
}

static void correctInheritedUsage(LoopGraph* firstLoop, LoopGraph* loop, int dimensions, set<SgSymbol*>& firstLoopVars, set<SgSymbol*>& loopVars)
{
    set<SgSymbol*> firstLoopIterationVars;
    fillIterationVariables(firstLoop, firstLoopIterationVars, dimensions);
    set<SgSymbol*> loopIterationVars;
    fillIterationVariables(loop, loopIterationVars, dimensions);

    LoopGraph* first = firstLoop;
    for (int i = 0; i < dimensions; ++i)
    {
        SgSymbol* var = getLoopSymbol(first);
        if (findSymbolById(loopVars, var->id()) != NULL && findSymbolById(loopIterationVars, var->id()) == NULL)
        {
            if (varIsChanged(var, loop))
            {
                if (isAntiVarDependency(var, (SgForStmt*)loop->loop))
                {
                    SgSymbol* newSymbol = copySymbolAndRename(var);
                    eraseSymbolById(loopVars, var->id());
                    loopVars.insert(newSymbol);
                    makeDeclaration(loop->loop, vector<SgSymbol*> { newSymbol });
                    replaceIterationVar(first, loop, dimensions, var, newSymbol);
                }
            }
            else
            {
                eraseSymbolById(loopVars, var->id());
                changeIterationVarToCountExpr(first, loop, dimensions, var);
            }
        }

        if (i != dimensions - 1)
            first = first->children[0];
    }
    
    for (int i = 0; i < dimensions; ++i)
    {
        SgSymbol* var = getLoopSymbol(loop);
        if (findSymbolById(firstLoopIterationVars, var->id()) == NULL)
        {
            SgExpression* countExpr = createIterationCountExpr(loop);
            if (findSymbolById(firstLoopVars, var->id()) != NULL)
            {
                SgStatement* st = new SgAssignStmt(*new SgVarRefExp(var), *countExpr);
                firstLoop->loop->insertStmtAfter(*st, *firstLoop->loop->controlParent());
            }
            else
            {
                SgStatement* st = new SgAssignStmt(*new SgVarRefExp(var), *countExpr);
                firstLoop->loop->insertStmtBefore(*st, *firstLoop->loop->controlParent());
            }
        }

        if (i != dimensions - 1)
            loop = loop->children[0];
    }
}

static int solveVarsCollisions(LoopGraph* firstLoop, LoopGraph* loop, int dimensions, map<LoopGraph*, set<SgSymbol*>>& mapPrivates)
{
    set<SgSymbol*> firstLoopAllVars;
    getAllVariables(firstLoop->loop, firstLoop->loop->lastNodeOfStmt(), set<int> { VAR_REF, ARRAY_REF }, firstLoopAllVars);
    set<SgSymbol*> loopAllVars;
    getAllVariables(loop->loop, loop->loop->lastNodeOfStmt(), set<int> { VAR_REF, ARRAY_REF }, loopAllVars);

    if (mapPrivates.find(loop) == mapPrivates.end())
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
    set<SgSymbol*> loopPrivates = mapPrivates[loop];

    if (mapPrivates.find(firstLoop) == mapPrivates.end())
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
    set<SgSymbol*> firstLoopPrivates = mapPrivates[firstLoop];

    set<SgSymbol*> loopIterationVars;
    fillIterationVariables(loop, loopIterationVars, dimensions);

    for (SgSymbol* var : firstLoopAllVars)
    {
        if (var->type()->variant() == T_ARRAY || !varIsReallyNotPrivate(var, firstLoop, mapPrivates))
            continue;
                
        int varId = var->id();
        if (findSymbolById(loopPrivates, varId) != NULL || findSymbolById(firstLoopPrivates, varId) != NULL)
            continue;

        bool isChangedInFirst = false, isChangedInSecond = false;
        bool isReadInFirst = false, isReadInSecond = false;
        if (findSymbolById(loopAllVars, varId) != NULL && varIsReallyNotPrivate(var, loop, mapPrivates))
        {
            isChangedInFirst = varIsChanged(var, firstLoop);
            isChangedInSecond = varIsChanged(var, loop);

            isReadInFirst = varIsRead(var, firstLoop);
            isReadInSecond = varIsRead(var, loop);

            if (isChangedInFirst && isReadInSecond || isChangedInSecond && isReadInFirst)
                return -1;
        }
    }

    correctInheritedUsage(firstLoop, loop, dimensions, firstLoopAllVars, loopAllVars);

    for (SgSymbol* var : loopPrivates)
        eraseSymbolById(firstLoopPrivates, var->id()); 

    for (SgSymbol* var : loopIterationVars)
    {
        eraseSymbolById(firstLoopPrivates, var->id());
        eraseSymbolById(loopPrivates, var->id());

        LoopGraph* parentLoop = loop;
        while (parentLoop)
        {
            auto pair = mapPrivates.find(parentLoop);
            if (pair == mapPrivates.end())
                printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
            
            eraseSymbolById(pair->second, var->id());
            parentLoop = parentLoop->parent;
        }
    }

    vector<SgSymbol*> symbolsToDeclare;

    set<SgSymbol*> varsFromLoopToRename;
    getIntersectionById(firstLoopAllVars, loopPrivates, varsFromLoopToRename);
    set<SgSymbol*> varsFromFirstLoopToRename;
    getIntersectionById(loopAllVars, firstLoopPrivates, varsFromFirstLoopToRename);

    map<SgSymbol*, SgSymbol*> symbolsToRename;
    for (SgSymbol* symbol : varsFromLoopToRename)
    {
        if (varIsReallyNotPrivate(symbol, firstLoop, mapPrivates))
        {
            SgSymbol* newSymbol = copySymbolAndRename(symbol);
            symbolsToDeclare.push_back(newSymbol);
            symbolsToRename.insert(make_pair(symbol, newSymbol));
        }
    }
    renamePrivatesInMap(loop, symbolsToRename, mapPrivates);
    renameVariablesInLoop(loop, symbolsToRename);

    symbolsToRename.clear();
    for (SgSymbol* symbol : varsFromFirstLoopToRename)
    {
        if (varIsReallyNotPrivate(symbol, loop, mapPrivates))
        {
            SgSymbol* newSymbol = copySymbolAndRename(symbol);
            symbolsToDeclare.push_back(newSymbol);
            symbolsToRename.insert(make_pair(symbol, newSymbol));
        }
    }
    renamePrivatesInMap(firstLoop, symbolsToRename, mapPrivates);
    renameVariablesInLoop(firstLoop, symbolsToRename);

    makeDeclaration(symbolsToDeclare, loop->loop->GetOriginal());
    
    LoopGraph* loopToInsert = firstLoop;
    for (int i = 0; i < dimensions; ++i)
    {
        auto pair = mapPrivates.find(loopToInsert);
        if (pair == mapPrivates.end())
            printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

        for (SgSymbol* privateVar : mapPrivates[loop])
            pair->second.insert(privateVar);
        
        if (i != dimensions - 1)
            loopToInsert = loopToInsert->children[0];
    }

    LoopGraph* toDelete = loop;
    for (int i = 0; i < dimensions; ++i)
    {
        mapPrivates.erase(toDelete);
        if (i != dimensions - 1)
            toDelete = toDelete->children[0];
    }

    return 0;
}

/**
 * Собственно объединение
 */
static bool combine(LoopGraph* firstLoop, const vector<LoopGraph*>& nextLoops, set<LoopGraph*>& combinedLoops,
                    map<LoopGraph*, set<SgSymbol*>>& mapPrivates, vector<Messages>& messages)
{
    bool wasCombine = false;
    int perfectLoop;
    int dimensionsForCombine = 0;

    LoopGraph* curLoop;
    vector<SgForStmt*> basisLoops;
    SgForStmt* innerMainLoop;

    for (LoopGraph* loop : nextLoops)
    {
        perfectLoop = std::min(firstLoop->perfectLoop, loop->perfectLoop);
        curLoop = firstLoop;
        basisLoops.clear();
        for (int i = 0; i < perfectLoop; ++i)
        {
            SgForStmt* loopStmt = (SgForStmt*)(curLoop->loop->GetOriginal());
            basisLoops.push_back(loopStmt);
            if (curLoop->hasLimitsToSplit())
                return false;

            if (i != perfectLoop - 1)
                curLoop = curLoop->children[0];
        }
        
        map<SgSymbol*, SgSymbol*> symbolsFromLoopToRename;
        dimensionsForCombine = getDeepestDimForCombine(basisLoops, loop);
        LoopGraph* loopToReverse = NULL;
        if (dimensionsForCombine == 0)
            dimensionsForCombine = getDipestDimToReverse(firstLoop, loop, perfectLoop, &loopToReverse);

        if (dimensionsForCombine)
        {
            if (solveVarsCollisions(firstLoop, loop, dimensionsForCombine, mapPrivates) == -1)
                break;

            reverseLoop(loopToReverse, dimensionsForCombine);
            compareIterationVars(firstLoop, loop, dimensionsForCombine, symbolsFromLoopToRename);

            innerMainLoop = getInnerLoop(firstLoop, dimensionsForCombine);
            moveBody(getInnerLoop(loop, dimensionsForCombine), innerMainLoop, symbolsFromLoopToRename);

            if (loop->loop->comments())
                firstLoop->loop->addComment(string(loop->loop->comments()).c_str());

            loop->loop->extractStmt();
            combinedLoops.insert(loop);
            wasCombine = true;

            //move in structure
            LoopGraph* deep = loop, *parent = firstLoop;
            for (int p = 0; p < dimensionsForCombine - 1; ++p)
            {
                deep = deep->children[0];
                parent = parent->children[0];
            }

            for (auto& toMove : deep->children)
            {
                parent->children.push_back(toMove);
                toMove->parent = parent;
            }
            deep->children.clear();
            firstLoop->recalculatePerfect();

#ifdef _WIN32
            wstring strR, strE;
            __spf_printToLongBuf(strE, L"Loops on line %d and on line %d were combined", firstLoop->lineNum, loop->lineNum);
            __spf_printToLongBuf(strR, R100, firstLoop->lineNum, loop->lineNum);

            messages.push_back(Messages(NOTE, firstLoop->lineNum, strR, strE, 2005));
#endif
            __spf_print(1, "Loops on lines %d and %d were combined\n", firstLoop->lineNum, loop->lineNum);
        }
        else
            break;
    }

    return wasCombine;
}

/**
 * Возвращает следующие loopsAmount циклов после nextAfterThis.
 * Если loopsAmount < 0, вернёт все последующие циклы, до первого оператора-не-цикла.
 */
static vector<LoopGraph*> getNextLoops(LoopGraph* nextAfterThis, vector<LoopGraph*>& loops, int loopsAmount)
{
    vector<LoopGraph*> result;
    SgStatement* lastSt = nextAfterThis->loop->lastNodeOfStmt();

    int z = 0;
    for (; z < loops.size(); ++z)
        if (loops[z] == nextAfterThis)
            break;
    if (z == loops.size())
        return result;
    else
        z++;

    for (; z < loops.size(); ++z)
    {
        if (loopsAmount == 0)
            break;

        SgStatement* loopSt = loops[z]->loop->GetOriginal();
        if (lastSt->lexNext() != loopSt)
            break;
        else
        {
            lastSt = loopSt->lastNodeOfStmt();
            result.push_back(loops[z]);
            --loopsAmount;
        }
    }

    return result;
}



static bool tryToCombine(vector<LoopGraph*>& loopGraphs, map<LoopGraph*, set<SgSymbol*>>& mapPrivates, vector<Messages>& messages)
{
    if (loopGraphs.size() == 0)
        return false;

    bool change = false;
    set<LoopGraph*> loopsToDelete;
    vector<LoopGraph*> newloopGraphs;

    vector<LoopGraph*> loops = loopGraphs;
    for (size_t z = 0; z < loops.size(); ++z)
    {
        LoopGraph* loop = loops[z];
        newloopGraphs.push_back(loop);
        vector<LoopGraph*> nextLoops = getNextLoops(loop, loopGraphs, -1);

        set<LoopGraph*> combinedLoops;
        change = false;
        if (nextLoops.size())
            change = combine(loop, nextLoops, combinedLoops, mapPrivates, messages);

        for (LoopGraph* combined : combinedLoops)
        {
            loopsToDelete.insert(combined);
            loopGraphs.erase(find(loopGraphs.begin(), loopGraphs.end(), combined));
        }

        if (change)
        {
            LoopGraph* loopParent = loop;
            while (loopParent->parent)
                loopParent = loopParent->parent;
            addIterationVarsToMap(loopParent, mapPrivates);

            LoopGraph* outerParent = loop;
            while (outerParent->parent)
                outerParent = outerParent->parent;

            outerParent->recalculatePerfect();
        }

        z += combinedLoops.size();
    }
    loopGraphs = newloopGraphs;

    for (LoopGraph* elem : loopsToDelete)
        delete elem;

    if (change == false)
    {
        for (LoopGraph* ch : loopGraphs)
        {
            bool res = tryToCombine(ch->children, mapPrivates, messages);
            change |= res;
        }
    }
    return change;
}

int combineLoops(SgFile* file, vector<LoopGraph*>& loopGraphs, vector<Messages>& messages, const pair<string, int>& onPlace)
{
    map<int, LoopGraph*> mapGraph;
    createMapLoopGraph(loopGraphs, mapGraph);

    map<LoopGraph*, set<SgSymbol*>> mapPrivates;
    fillMapPrivateVars(loopGraphs, mapPrivates);

    if (onPlace.second > 0)
    {
        if (onPlace.first != file->filename())
            return 0;
        else
        {
            const int onLine = onPlace.second;

            auto it = mapGraph.find(onLine);
            if (it == mapGraph.end())
                printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

            vector<LoopGraph*> nextLoops = getNextLoops(it->second, it->second->parent ? it->second->parent->children : loopGraphs, 1);
            set<LoopGraph*> combinedLoops;
            if (nextLoops.size())
                combine(it->second, nextLoops, combinedLoops, mapPrivates, messages);

            return 0;
        }
    }

    bool change = true;
    while (change)
        change = tryToCombine(loopGraphs, mapPrivates, messages);

    return 0;
}